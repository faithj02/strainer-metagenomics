# Strainer-Metagenomics

## Future development
Development has now moved to Strainer2 on github, which should be more hpc friendly with modular components to facilitate continued improvements to detection performance.

https://github.com/jeremiahfaith/strainer2

## Readme for Strainer algorithm for strain detection and tracking.
The Strainer algorithm is a series of python programs that use the kmc kmer software to identify informative (unique) kmers in a bacterial strain's genome and use them to track that bacterial strain in metagenomics data. 
We explain the usage through two important potential applications and provide several datasets for the community to benchmark and test their tools for strain detection/tracking.

1.	[Accurate detection of a set of strains in the gut microbiome samples from several unrelated individuals](#Application1)
2.	[Accurate tracking of a set of bacterial strains from the donor to recipient post-FMT](#Application2)
3.  [Comparison with metagenomics based strain clustering algorithms that attempt to track strains AND ASSOCIATED GOLD STANDARD DATA](#Dataset1)
4.  [Bacterial strains cultured from healthy donors, patients with IBD, and patients with rCDI alongwith the corresponding metagenomes](#Dataset2)
5.  [FMT Donor and Recipient samples and their timepoints, corresponding fecal metagenomics samples and the strains cultured from it](#Dataset3)


Software Dependencies:

- Python version 2.7.16, along with package Numpy, HTSeq (for reading fasta files)
- Trimmomatic version 0.36
- KMC k-mer counting utility version 3.1.0
- Bowtie version 2.2.8


If you have any questions then do not hesitate to contact me (Varun Aggarwala varunaggarwala AT gmail) or Prof. Faith.

## Application 1: Accurate detection of a set of strains in the gut microbiome samples from several unrelated individuals. {#Application1} 

In this test example, we have 15 sequenced bacterial isolates from 5 different unrelated individuals. Each of the bacterial isolates was isolated/cultured from one of the five individuals and is not present in either of the remaining four individuals. Therefore, we know the correct individual from which it was isolated. From this knowledge, we can make a truth table that maps each strain to the right individual, and the goal of the Strain algorithm is to reconstruct the truth table from an analysis of the genomes and the metagenomes.

A potential real world application of this example can be to detect the successful colonization of several probiotic strains from gut metagenomes of different individuals.

### Downloading the dataset and preparing the directory structure

1. Create main project directory Strainer_data_code and subdirectories data, analysis, scrub_databases, code

   - `mkdir Strainer_data_code`
   - `mkdir Strainer_data_code/code`
   - `mkdir Strainer_data_code/data`
   - `mkdir Strainer_data_code/analysis`
   - `mkdir Strainer_data_code/scrub_databases`
   
2. Download the compressed data archive and move it in the folder Strainer_data_code/data 

   1. Get this file containing metagenomes and strains from a sample.
      - `wget https://aggarv01.u.hpc.mssm.edu/strainer_data/strain_detection_unrelated_metagenomes.tar.gz` (4.6 gb)
   2. Uncompress this file
      - `tar -zxvf strain_detection_unrelated_metagenomes.tar.gz`
   3. This creates a new folder strain_detection_unrelated_metagenomes, which has subfolders of metagenomes/ and strains/, and a truth_table file. metagenomes/ contains 5 metagenomics samples (paired-end so 10 total files) and strains/ contains the 15 genomes each of which are from bacteria isolated from one of the 5 individuals from which the metagenome was generated. 
   
   It also has a file truth_table where bacteria are rows and metagenomes are columns; 0 = not present; 1 = present; The algorithm will try to recreate this file, independently from just the metagenomics and strain genomes.

4. Download the (LARGE!) scrubbing databases into the folder Strainer_data_code/scrub_databases.
 
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/merge_0_1_2_3_4.kmc_suf` (8 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/merge_0_1_2_3_4.kmc_pre` (351 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/pangenomes_ncbi2018-11-20_kmcdb.kmc_suf` (1.3 mb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/pangenomes_ncbi2018-11-20_kmcdb.kmc_pre` (98 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/russian_metagenomes_kmerlen_31_kmcdb.kmc_pre` (1.3 mb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/russian_metagenomes_kmerlen_31_kmcdb.kmc_suf` (29 gb)

Note that the original metagenomics files have been processed by trimmomatic and then subsampled into 10M reads. Code is provided for this, but because of space considerations (and ease for the end-user), we are directly providing the fasta files. The provided code requires the sequencing reads in the fasta file to have names in a specific format (SampleName_part_NUM, where NUM corresponds to the read number in the fasta file). This reduces the memory footprint in the subsequent analysis, as we can pull reads by the number. If you are using your own fasta file, then use our utility to rename the reads.

## Processing bacterial isolates to find unique strains.

We consider two bacterial isolates with >=96% sequence similarity to represent isolates from a single unique bacterial strain. For the mapping of strains to metagenomes, we only want to work with one isolate of each strain to eliminate redundancies. Here we download these components (from bitbucket for the software and from the Icahn School of Medicine at Mount Sinai for the larger files). Make sure each downloaded file is placed in the appropriate directory.

1. Download the compressed code file `wget https://bitbucket.org/faithj02/strainer-metagenomics/src/master/packages/strain_detection_unrelated_metagenomes.tar.gz`

  - Uncompress this file in the folder Strainer_data_code/code/
  
    - `tar -zxvf strain_detection_unrelated_metagenomes.tar.gz`
    - This creates a new folder strain_detection_unrelated_metagenomes/
    - Go into the directory Strainer_data_code/code/strain_detection_unrelated_metagenomes
    
2. We first generate k-mers (k=31) for all the strains and save them in dump files in their respective folders

  - `python find_kmcdb_dump_strains.py`
    - creates a subfolder of ../../analysis/strain_detection_unrelated_metagenomes/, and ../../analysis/strain_detection_unrelated_metagenomes/strains/, and saves the kmc k-mer database files and a dump file StrainName_kmcdb_dump which is a tabular file of each k-mer and count in that strain

3. We next compare each isolate with each other, to find the pairwise similarity and then choose strains that are <96% pairwise similar to each other.

  - `python compare_fmt_genomes_one_against_all_wrapper.py`
    - generates the command file jobssubmit_compare_fmt_genomes_one_against_all containing individual calls to file compare_fmt_genomes_one_against_all.py on each strain
  - `sh jobssubmit_compare_fmt_genomes_one_against_all`
    - creates the directory ../../analysis/strain_detection_unrelated_metagenomes/pairwise_comparisons, and individual files StrainName_pairwise_results_kmer_approach_against_all which contains the strain comparisons with each other
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster
  - `python compare_fmt_genomes_one_against_all_merge.py`
    - combines all the pairwise comparison files generated before into a single file ../../analysis/strain_detection_unrelated_metagenomes/pairwise_comparisons/pairwise_results_kmer_approach_masterfile
    
4. Next we run the script find isolates >96% similar to each other, consider them as one and work with a unique strain.

  - `python fmt_genomes_deduplicated.py`
    - In this example, most isolates are <96% similar to others. But here for 2 of them there are multiple isolates:
      Bacteroides_fragilis_12, Bacteroides_fragilis_13 (more than 96% similar), we only consider 1 i.e. Bacteroides_fragilis_12 for future analysis
      Bacteroides_ovatus_1, Bacteroides_ovatus_2 (more than 96% similar), we only consider 1 i.e. Bacteroides_ovatus_1 for future analysis
    - This script generates the directory ../../analysis/strain_detection_unrelated_metagenomes/deduplicated_strains/ which contains the link for only those strains that are unique (i.e. deduplicated)
    - It also creates a file ../../analysis/strain_detection_unrelated_metagenomes/pairwise_comparisons/master_file_strain, which has for each isolate the unique strain that was considered.

## Identifying k-mers for a strain that are informative and unique.

We try to remove those k-mers for a strain that are found in other unrelated metagenomes, pangenomes and de-duplicated strains from this study

1. We scrub each strain against 2 metagenome k-mer databases from unrelated Russian and Mongolian populations.

  - `python scrub_deduplicated_strains_metagenomes.py`
    - This script creates a folder ../../analysis/strain_detection_unrelated_metagenomes/scrub_deduplicated_strains_metagenomes/, where the dump file (k-mer present in the strain that are also observed in the unrelated metagenomes, and count of their combined occurrences) is saved.
    - The script generates another batch script jobssubmit_scrub_deduplicated_strains_metagenomes with commands for each strain.
  - `sh jobssubmit_scrub_deduplicated_strains_metagenomes`
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 1 hours of compute time per strain and 12GB of RAM.
    
2. We scrub each strain against a combined pangenome k-mer database from unrelated >10K strains from NCBI.

  - `python scrub_deduplicated_strains_pangenomes.py`
    - This script creates a folder ../../analysis/strain_detection_unrelated_metagenomes/scrub_deduplicated_strains_pangenomes/, where the dump file (k-mer present in the strain that are also observed in the unrelated pangenomes, and count of their combined occurrences) is saved.
    - The script generates another batch script jobssubmit_scrub_deduplicated_strains_pangenomes with commands for each strain.
  - `sh jobssubmit_scrub_deduplicated_strains_pangenomes`
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 30 minutes of compute time per strain and 12GB of RAM.
    
3. We then scrub each strain against all of the strains in the dataset (in this case the 13 deduplicated strains)

  - `python scrub_deduplicated_strains_otherstrains_wrapper.py`
    - This script creates a folder ../../analysis/strain_detection_unrelated_metagenomes/scrub_deduplicated_strains_otherstrains/, where each strain is compared to other de-duplicated strains. Finally we save the dump file of these kmer comparisons, which contains the k-mer present in the strain that are also observed in the unrelated strains, and count of their combined occurrences.
    - `sh jobssubmit_scrub_deduplicated_strains_otherstrains`
      - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 10 minutes of compute time per strain and 2GB of RAM.
      - note the scrubbing with the pangenomes, metagenomes, and local strains is independent (i.e., can be run on a computing cluster at the same time)
      
4. We next combine these scrubbed files to find informative k-mers belonging to each strain. We removes k-mers that are consistently observed (beyond a threshold) in each of the dump files (k-mers for a strain also observed in unrelated databases, suggesting that they are non-informative and not unique) in the previous steps. This threshold is dynamically chosen for each database and strain, such that we do not scrub away all k-mers or leave too few of them.

  - `python scrub_deduplicated_strains_Allapproach.py`
    - This creates a new folder ../../analysis/strain_detection_unrelated_metagenomes/scrub_deduplicated_strains_ALL/ and creates the scrubbed dump files.
    - The output is the number of original kmers, % left after scrubing against other strains, % left after scrubbing against metagenomes, % left after scrubbing against pangenomes.
  - python scrub_deduplicated_strains_Allapproach_getids.py
    - saves for each kmer (for a strain) in the scrubbed dump file, the line number from the original (unscrubbed) strain kmer dump file. We save the line numbers because they have less memory footprint than the k-mer.
    
## Initial read mapping of informative kmers to metagenome to identify informative reads

1. For each strain, we find the occurrence of its k-mers in the metagenomic sample file.

  - `python find_sample_test_multiple_strains_wrapper.py`
    - generates a batch job file to run find_sample_test_multiple_strains.py on a specific metagenome and (set of) strains.
    - The underlying idea is to save for each metagenomic sample and strain, the sequencing reads which have k-mers belonging to a strain. We will then further refine these saved files.
    - creates a folder ../../analysis/strain_detection_unrelated_metagenomes/sample_test_rawstrains/ . Each resulting file saves the read, and occurrences of each k-mer for a strain in that read (if it exists).
    - the format of the files is READ_NUM PE_ | KMER_LINEID COUNT | where READ_NUM is the read number in the metagenomics file, PE_ is the paired-end indicator (1 = paired-end 1; 2 = paired-end 2); KMER_LINEID is the kmer number in the scrubbed kmer file and COUNT is the number of times the kmer was in the particular read. Only reads with at least one kmer match across the two paired-end reads are saved.
  - `sh jobssubmit_test_multiple_strains`
    - this batch file format makes it possible to run the comparisons in parallel with high performance computing . You will need approximately 8 hour of compute time per sample and 2GB of RAM.

2. Next we assign sequencing reads uniquely to each strain for each metagenomic sample. This problem is challenging because k-mers belonging to multiple strains might co-occur on a same read. In this case, we discard such sequencing reads, and also remove the k-mers present in this read from the earlier set of informative and scrubbed k-mers found before. We also find and remove k-mers belonging to a strain that are present in sequencing reads from unrelated metagenomes, because they likely represent genomic regions of a strain that is non-unique and uninformative. Because of considerations of memory and speed, and the fact that we have to iteratively repeat the process of allocation of strains to a read after some k-mers have been deemed un-informative, we perform this using 2 scripts

  - `python understand_sample_test_individual_strain_raw_results_wrapper.py`
    - creates the subfolder ./../analysis/strain_detection_unrelated_metagenomes/sample_test_rawstrains/scratch_delete and saves the kmers seen for each strain in the metagenomic samples, legitimate reads seen for this strain in the metagenomic samples.
    - creates a batch job file invoking understand_sample_test_individual_strain_raw_results.py on each strain.
    - `sh jobssubmit_understand_sample_test_individual_strain`
  - `python understand_sample_test_allstrains_nextstep_raw_results.py`
    - takes the first pass of results from the script understand_sample_test_individual_strain_raw_results.py on each strain, and assigns sequencing reads uniquely to each strain
    - it creates the files ../../analysis/strain_detection_unrelated_metagenomes/results_raw and ../../analysis/strain_detection_unrelated_metagenomes/results_raw_reads which contains the reads assigned to each strain, and the corresponding read ids to map in the next step.
    - the results_raw file has the format strain Sample1_reads Sample2_reads etc.... providing the counts of all the informativeReads mapping each strain in each metagenome prior to normalizing for coverage (done below)
    - the results_raw_reads files have the format like: Sample2_part_18078717 Sample2 Bacteroides_fragilis_12 121 102 Sample2_part_18078717 means the sequencing read is informative, it belongs to Sample2, read 18078717, it is uniquely assigned to strain Bacteroides_fragilis_12 with 121 kmer mapping to read 1 and 102 mapping to read 2

## Mapping informative reads to strain genomes to quantify genome coverage uniformity

We now map the assigned reads back to the strain to test for relative uniform distribution of reads across the strain, and also normalize for different sequencing depths by considering a single read (out of multiple read pileup). We also find reads from samples that are not related to each other, yet map to the same location on a strain. This suggesting that they are not unique genomic regions, and we ignore such reads.

1. `python map_reads_ofstrain_tosamples_wrapper.py`
  - generates a batch job file jobssubmit_map_reads_ofstrain_tosamples
2. `sh jobssubmit_map_reads_ofstrain_tosamples`
  - runs script map_reads_ofstrain_tosamples.py for each strain and maps all assigned reads back to it.
  - creates the folder ../../analysis/strain_detection_unrelated_metagenomes/reads_mapback/ which has the bowtie files for each strain, sam files after mapping the assigned sequencing reads to this strain, and a overall summary file for each strain which details the reads in a metagenomic sample and those present in unrelated "conflict" samples, for all the possible metagenomic samples.
3. `python analyze_results_map_reads_ofstrain_tosamples.py`
  - use the mapped reads for a strain in each metagenomic sample to compare it with those in unrelated negative controls (or conflict samples), to present confidence scores for the presence or absence of the strain in each sample.
  - The script takes the mapped reads for each strain onto different metagenomic samples, compares them with unrelated negative controls and presents the final confidence scores in the file ../../analysis/strain_detection_unrelated_metagenomes/results_readdistribution_actualreads_confidencescores. The processed mapped reads are saved in the file ../../analysis/strain_detection_unrelated_metagenomes/results_readdistribution_actualreads.

    
## Application 2: Accurate tracking of a set of bacterial strains from the donor to recipient post-FMT {#Application2}

Here, we have 49 sequenced bacterial isolates from the Donor, Recipient before FMT and at 8-weeks after FMT. Since, we have isolated these bacteria from a specific individual, we know the truth and correct individual from which it was isolated, but we want to query if we can independently detect them from metagenomics.

Moreover, here we also have multiple metagenomics samples from the recipient, and we want to query if these strains were also present at other timepoints, so basically we want to track all the bacterial strains in the recipient and the donor. 
A potential real world application of this example can be to understand the engraftment of donor material into recipients and choose the successful engrafters for designing live biotherapeutics as a safer, scalable alternative to whole stool FMT, and to find candidate strains for followup.

### Downloading the dataset and preparing the directory structure

1. Create main project directory Strainer_data_code and subdirectories data, analysis, scrub_databases, code

   - `mkdir Strainer_data_code`
   - `mkdir Strainer_data_code/code`
   - `mkdir Strainer_data_code/data`
   - `mkdir Strainer_data_code/analysis`
   - `mkdir Strainer_data_code/scrub_databases`
   
2. Download the compressed data archive and move it in the folder Strainer_data_code/data 

   1. Get this file containing metagenomes and strains from a sample.
      - `wget https://aggarv01.u.hpc.mssm.edu/strainer_data/strain_tracking_FMT.tar.gz.tar.gz` (7.4 gb)
   2. Uncompress this file
      - `tar -zxvf strain_tracking_FMT.tar.gz`
   3. This creates a new folder strain_tracking_FMT, which has subfolders of metagenomes/ and strains/, and a truth_table file. metagenomes/ contains 12 metagenomics samples (paired-end so 24 total files) and strains/ contains the 49 genomes each of which are from bacteria isolated from either the Donor_before_FMT or Recipient_before_FMT or Recipient_8weeks_postFMT or Recipient_1year_postFMT samples. 
   
   It also has a file truth_table where bacteria are rows and metagenomes are columns; 0 = not present; 1 = present and cultured from; 2 = maybe present (i.e. isolated from recipient at 8 weeks so maybe present at 1 year or 4 weeks too) The algorithm will try to recreate this file, independently from just the metagenomics and strain genomes. 0 means that it can never be present in a sample, i.e. the strains from the donor cannot be present in recipient prior to FMT or in unrelated Negative controls. 
   
   It also has a file conflict_table which lists for each sample its conflicts, i.e. no possible relationship. For example, negative controls are in conflict with every sample. We need such a file to filter away k-mers, i.e. k-mers of a strain shared between samples that are in conflict suggest that these k-mers are not informative as they are promiscuously shared across the phylogenetic tree.
   
   It also has files FMT_samples and FMT_strains that contains the metagenomic samples and strains that we described earlier. Providing the files FMT_samples, FMT_strains, conflict_table and the fast(a/q) reads and genomes is a hard requirement.

4. Download the (LARGE!) scrubbing databases into the folder Strainer_data_code/scrub_databases.
 
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/merge_0_1_2_3_4.kmc_suf` (8 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/merge_0_1_2_3_4.kmc_pre` (351 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/pangenomes_ncbi2018-11-20_kmcdb.kmc_suf` (1.3 mb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/pangenomes_ncbi2018-11-20_kmcdb.kmc_pre` (98 gb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/russian_metagenomes_kmerlen_31_kmcdb.kmc_pre` (1.3 mb)
  - `wget https://faithj02.u.hpc.mssm.edu/strainer_data/russian_metagenomes_kmerlen_31_kmcdb.kmc_suf` (29 gb)

Note that the original metagenomics files have been processed by trimmomatic and then subsampled into 10M reads. Code is provided for this, but because of space considerations (and ease for the end-user), we are directly providing the fasta files. The provided code requires the sequencing reads in the fasta file to have names in a specific format (SampleName_part_NUM, where NUM corresponds to the read number in the fasta file). This reduces the memory footprint in the subsequent analysis, as we can pull reads by the number. If you are using your own fasta file, then use our utility to rename the reads.

## Processing bacterial isolates to find unique strains.

We consider two bacterial isolates with >=96% sequence similarity to represent isolates from a single unique bacterial strain. For the mapping of strains to metagenomes, we only want to work with one isolate of each strain to eliminate redundancies. Here we download these components (from bitbucket for the software and from the Icahn School of Medicine at Mount Sinai for the larger files). Make sure each downloaded file is placed in the appropriate directory.

1. Download the compressed code file `wget https://bitbucket.org/faithj02/strainer-metagenomics/src/master/packages/strain_tracking_FMT.tar.gz`

  - Uncompress this file in the folder Strainer_data_code/code/
    - `tar -zxvf strain_tracking_FMT.tar.gz`
    - This creates a new folder strain_tracking_FMT/
    - Go into the directory Strainer_data_code/code/strain_tracking_FMT
    
2. We first generate k-mers (k=31) for all the strains and save them in dump files in their respective folders

  - `python find_kmcdb_dump_strains.py`
    - creates a subfolder of ../../analysis/strain_tracking_FMT/, and ../../analysis/strain_tracking_FMT/strains/, and saves the kmc k-mer database files and a dump file StrainName_kmcdb_dump which is a tabular file of each k-mer and count in that strain

3. We next compare each isolate with each other, to find the pairwise similarity and then choose strains that are <96% pairwise similar to each other.

  - `python compare_fmt_genomes_one_against_all_wrapper.py`
    - generates the command file jobssubmit_compare_fmt_genomes_one_against_all containing individual calls to file compare_fmt_genomes_one_against_all.py on each strain
  - `sh jobssubmit_compare_fmt_genomes_one_against_all`
    - creates the directory ../../analysis/strain_tracking_FMT/pairwise_comparisons, and individual files StrainName_pairwise_results_kmer_approach_against_all which contains the strain comparisons with each other
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster
  - `python compare_fmt_genomes_one_against_all_merge.py`
    - combines all the pairwise comparison files generated before into a single file ../../analysis/strain_tracking_FMT/pairwise_comparisons/pairwise_results_kmer_approach_masterfile
    
4. Next we run the script find isolates >96% similar to each other, consider them as one and work with a unique strain.

  - `python fmt_genomes_deduplicated.py`
    - In this example, every isolate is <96% similar to others, suggesting that we have all 49 unique strains 
    - This script generates the directory ../../analysis/strain_tracking_FMT/deduplicated_strains/ which contains the link for only those strains that are unique (i.e. deduplicated)
    - It also creates a file ../../analysis/strain_tracking_FMT/pairwise_comparisons/master_file_strain, which has for each isolate the unique strain that was considered.

## Identifying k-mers for a strain that are informative and unique.

We try to remove those k-mers for a strain that are found in other unrelated metagenomes, pangenomes and de-duplicated strains from this study

1. We scrub each strain against 2 metagenome k-mer databases from unrelated Russian and Mongolian populations.

  - `python scrub_deduplicated_strains_metagenomes.py`
    - This script creates a folder ../../analysis/strain_tracking_FMT/scrub_deduplicated_strains_metagenomes/, where the dump file (k-mer present in the strain that are also observed in the unrelated metagenomes, and count of their combined occurrences) is saved.
    - The script generates another batch script jobssubmit_scrub_deduplicated_strains_metagenomes with commands for each strain.
  - `sh jobssubmit_scrub_deduplicated_strains_metagenomes`
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 1 hours of compute time per strain and 12GB of RAM.
    
2. We scrub each strain against a combined pangenome k-mer database from unrelated >10K strains from NCBI.

  - `python scrub_deduplicated_strains_pangenomes.py`
    - This script creates a folder ../../analysis/strain_tracking_FMT/scrub_deduplicated_strains_pangenomes/, where the dump file (k-mer present in the strain that are also observed in the unrelated pangenomes, and count of their combined occurrences) is saved.
    - The script generates another batch script jobssubmit_scrub_deduplicated_strains_pangenomes with commands for each strain.
  - `sh jobssubmit_scrub_deduplicated_strains_pangenomes`
    - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 30 minutes of compute time per strain and 12GB of RAM.
    
3. We then scrub each strain against all of the strains in the dataset (in this case the 49 deduplicated strains)

  - `python scrub_deduplicated_strains_otherstrains_wrapper.py`
    - This script creates a folder ../../analysis/strain_tracking_FMT/scrub_deduplicated_strains_otherstrains/, where each strain is compared to other de-duplicated strains. Finally we save the dump file of these kmer comparisons, which contains the k-mer present in the strain that are also observed in the unrelated strains, and count of their combined occurrences.
    - `sh jobssubmit_scrub_deduplicated_strains_otherstrains`
      - this batch file format makes it possible to split the comparisons across multiple CPU or across a high performance computing cluster. You will need approximately 10 minutes of compute time per strain and 2GB of RAM.
      - note the scrubbing with the pangenomes, metagenomes, and local strains is independent (i.e., can be run on a computing cluster at the same time)
      
4. We next combine these scrubbed files to find informative k-mers belonging to each strain. We removes k-mers that are consistently observed (beyond a threshold) in each of the dump files (k-mers for a strain also observed in unrelated databases, suggesting that they are non-informative and not unique) in the previous steps. This threshold is dynamically chosen for each database and strain, such that we do not scrub away all k-mers or leave too few of them.

  - `python scrub_deduplicated_strains_Allapproach.py`
    - This creates a new folder ../../analysis/strain_tracking_FMT/scrub_deduplicated_strains_ALL/ and creates the scrubbed dump files.
    - The output is the number of original kmers, % left after scrubing against other strains, % left after scrubbing against metagenomes, % left after scrubbing against pangenomes.
  - python scrub_deduplicated_strains_Allapproach_getids.py
    - saves for each kmer (for a strain) in the scrubbed dump file, the line number from the original (unscrubbed) strain kmer dump file. We save the line numbers because they have less memory footprint than the k-mer.
    
## Initial read mapping of informative kmers to metagenome to identify informative reads

1. For each strain, we find the occurrence of its k-mers in the metagenomic sample file.

  - `python find_sample_test_multiple_strains_wrapper.py`
    - generates a batch job file to run find_sample_test_multiple_strains.py on a specific metagenome and (set of) strains.
    - The underlying idea is to save for each metagenomic sample and strain, the sequencing reads which have k-mers belonging to a strain. We will then further refine these saved files.
    - creates a folder ../../analysis/strain_tracking_FMT/sample_test_rawstrains/ . Each resulting file saves the read, and occurrences of each k-mer for a strain in that read (if it exists).
    - the format of the files is READ_NUM PE_ | KMER_LINEID COUNT | where READ_NUM is the read number in the metagenomics file, PE_ is the paired-end indicator (1 = paired-end 1; 2 = paired-end 2); KMER_LINEID is the kmer number in the scrubbed kmer file and COUNT is the number of times the kmer was in the particular read. Only reads with at least one kmer match across the two paired-end reads are saved.
  - `sh jobssubmit_test_multiple_strains`
    - this batch file format makes it possible to run the comparisons in parallel with high performance computing . You will need approximately 8 hour of compute time per sample and 2GB of RAM.

2. Next we assign sequencing reads uniquely to each strain for each metagenomic sample. This problem is challenging because k-mers belonging to multiple strains might co-occur on a same read. In this case, we discard such sequencing reads, and also remove the k-mers present in this read from the earlier set of informative and scrubbed k-mers found before. We also find and remove k-mers belonging to a strain that are present in sequencing reads from unrelated metagenomes, because they likely represent genomic regions of a strain that is non-unique and uninformative. Because of considerations of memory and speed, and the fact that we have to iteratively repeat the process of allocation of strains to a read after some k-mers have been deemed un-informative, we perform this using 2 scripts

  - `python understand_sample_test_individual_strain_raw_results_wrapper.py`
    - creates the subfolder ./../analysis/strain_tracking_FMT/sample_test_rawstrains/scratch_delete and saves the kmers seen for each strain in the metagenomic samples, legitimate reads seen for this strain in the metagenomic samples.
    - creates a batch job file invoking understand_sample_test_individual_strain_raw_results.py on each strain.
    - `sh jobssubmit_understand_sample_test_individual_strain`
  - `python understand_sample_test_allstrains_nextstep_raw_results.py`
    - takes the first pass of results from the script understand_sample_test_individual_strain_raw_results.py on each strain, and assigns sequencing reads uniquely to each strain
    - it creates the files ../../analysis/strain_tracking_FMT/results_raw and ../../analysis/strain_tracking_FMT/results_raw_reads which contains the reads assigned to each strain, and the corresponding read ids to map in the next step.
    - the results_raw file has the format strain Sample1_reads Sample2_reads etc.... providing the counts of all the informativeReads mapping each strain in each metagenome prior to normalizing for coverage (done below)
    - the results_raw_reads files have the format like: Sample2_part_18078717 Sample2 Bacteroides_fragilis_12 121 102 Sample2_part_18078717 means the sequencing read is informative, it belongs to Sample2, read 18078717, it is uniquely assigned to strain Bacteroides_fragilis_12 with 121 kmer mapping to read 1 and 102 mapping to read 2

## Mapping informative reads to strain genomes to quantify genome coverage uniformity

We now map the assigned reads back to the strain to test for relative uniform distribution of reads across the strain, and also normalize for different sequencing depths by considering a single read (out of multiple read pileup). We also find reads from samples that are not related to each other, yet map to the same location on a strain. This suggesting that they are not unique genomic regions, and we ignore such reads.

1. `python map_reads_ofstrain_tosamples_wrapper.py`

  - generates a batch job file jobssubmit_map_reads_ofstrain_tosamples
2. `sh jobssubmit_map_reads_ofstrain_tosamples`

  - runs script map_reads_ofstrain_tosamples.py for each strain and maps all assigned reads back to it.
  - creates the folder ../../analysis/strain_tracking_FMT/reads_mapback/ which has the bowtie files for each strain, sam files after mapping the assigned sequencing reads to this strain, and a overall summary file for each strain which details the reads in a metagenomic sample and those present in unrelated "conflict" samples, for all the possible metagenomic samples.
3. `python analyze_results_map_reads_ofstrain_tosamples.py`

  - use the mapped reads for a strain in each metagenomic sample to compare it with those in unrelated negative controls (or conflict samples), to present confidence scores for the presence or absence of the strain in each sample.
  - The script takes the mapped reads for each strain onto different metagenomic samples, compares them with unrelated negative controls and presents the final confidence scores in the file ../../analysis/strain_tracking_FMT/results_readdistribution_actualreads_confidencescores. The processed mapped reads are saved in the file ../../analysis/strain_tracking_FMT/results_readdistribution_actualreads.

# Comparison with metagenomics based strain clustering algorithms that attempt to track strains {#Dataset1}

- For testing additional strain tracking algorithms (especially ones that do not use reference genomes and use metagenomes directly), it is more challenging to define "truth". Therefore we made more restrictive test datasets where the composition of the strains (or at least a subset of strains) in a community was known by either gavaging the strains into gnotobiotic mice or by culturing and sequencing the same isolate from multiple samples collected longitudinal from a single individual or from FMT donors and their recipients. 
 
- For these three test datasets, we also provide the unassembled reference genomes of the gold-standard strains that are used to determine if the algorithm detects and tracks the correct strains. See the README_comparison.rtf file for a description of the directories, files, and truth table. The underlying idea is to evaluate if the clustering based algorithms can correctly cluster the unassembled reads for a gold standard strain with the corresponding metagenomics samples from which it was cultured. For example if the algorithm can cluster the unassembled reads for a strain with the right donor and recipient (from which it was cultured), then it can be considered as a true positive. Note: Strainer can directly track such strains from metagenomics samples, but since the clustering based strain tracking algorithms (inStrains, Strain Finder, Constrains) do not output the discrete strain so we have created this gold standard dataset to benchmark these algorithms.
  
`wget https://aggarv01.u.hpc.mssm.edu/strainer_data/comparison_data_between_algorithmsstrain_tracking.tar`

# Bacterial strains cultured from healthy donors, patients with IBD, and patients with rCDI alongwith the corresponding metagenomes {#Dataset2}

- We provide sequenced fecal metagenomes and the strains cultured from it from a set of 10 healthy donors, 4 patients with IBD and 5 with rCDI. Each strain is associated with a specific sample from which it was cultured. 

- The dataset has been assembled to assist in the development and validation of algorithms to detect bacterial strains within a metagenome. The obvious goal will be to match the strain with its correct metagenomics sample. However, it could likely be used for multiple purposes such as the validation of metagenomic assembly algoritms, since a perfect metagenome assembler should recover the full genome of each of the isolates assuming their relative abundance is sufficient to have ample coverage. All folders have two subfolders: (1) metagenomes and (2) strains that contain the metagenomic sequencing files and genome sequences respectively. Each folder also has a file *_truth_table.txt that details, which strains were isolated from each individual (and thus are likely detectable in the corresponding metagenome).  The integer 1 denotes if a genome has been isolated from a particular sample and 0 denotes that it was not isolated from each individual.  None of the fecal samples are related to each other (within or across each category; for example no FMT donors and their recipients and no longitudinal samples), and therefore we do not expect any samples to share strains with each other (within or across each category).  For example if cdi_patients_Strain_XYZ is present in cdi_patients_Sample_ABC then it should not be present in any other cdi_patients_Sample_ or healthy_donors_Sample_ or ibd_patients_Sample_ Only a single strain, across the 369 strains in this compendium, was isolated from two individuals. The remaining strains were only isolated from a single individual. This lack of shared strains extends across all of the dataset, therefore the three datasets and truth_tables could be combined to generate a larger challenge or genomes from one challenge could be used as "decoys" in another challenge.

`wget https://aggarv01.u.hpc.mssm.edu/strainer_data/strain_challenge_mega.tar.gz` (17 gb)

# FMT Donor and Recipient samples and their timepoints, corresponding fecal metagenomics samples and the strains cultured from it {#Dataset3}

- We also provide a list of FMT donors and corresponding recipients with their timepoints, linked metagenomics samples and the strains cultured from them with detailed metadata. 
- We have provided this in the original publication (metadata with biosample ids), but we want to make the data access easier by providing it directly here.
- The directory contains 

    1.  master file with a metagenomics_sample_id, followed by metadata (donor (and samples where it was used) or recipients (and the donor it came from)), and timepoint (days before or after FMT) 
    2.  master file with metagenomics_sample_id and the strains_id that were cultured from that fecal sample 
    3.  directory of paired fasta files of the metagenomes that correspond to the metagenomics_sample_id
    4.  directory of the reference strain genomes that correspond to the strains_id. One can find which sample they have been cultured from by querying the corresponding master file
